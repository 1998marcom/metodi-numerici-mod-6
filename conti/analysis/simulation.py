from pathlib import Path
import numpy as np
import subprocess as s
from tqdm import tqdm

main_exe = '' # To be initialized on module load

class Simulation:
    
    path = ""
    runned = False
    kwargs = {}
    data = {
        'u': np.array([]),
    }
    
    def __init__(self, path):
        self.path = Path(path)
        if self.path.exists():
            self._load()
            self.runned = True
            
    def run(self, **kwargs):
        params = {('-'+key): val for key, val in kwargs.items()}
        params['-o'] = str(self.path.resolve())
        run_args = [str(elem) for keyval in params.items() for elem in keyval]
        run_args = [str(Path(main_exe).resolve()), *run_args]
        #print(run_args)
        s.run(run_args)
        self.runned = True
        self._load()
    
    def _load(self):
        hf_path = Path(str(self.path) + '_header').resolve()
        with open(hf_path, 'r') as hf:
            keys = hf.readline().split()
            values = hf.readline().split()
            assert len(keys) == len(values)
            top = len(keys)
            self.kwargs = {}
            for i in range(top):
                self.kwargs[keys[i]] = values[i]
        self.data = {
            'u': np.loadtxt(self.path, unpack=True),
        }

class Span:
    
    path = ""
    runned = False
    simulations = []
    
    def __init__(self, path):
        self.path = Path(path)
        if self.path.exists():
            self._load()
            self.runned = True
            
    def run(self, key, values, **kwargs):
        self.path.mkdir(exist_ok=True)
        for child in self.path.iterdir():
            child.unlink()
        params = kwargs
        print("Running the simulations...")
        for value in tqdm(values):
            params[key] = value
            sim = Simulation( self.path / (str(key) + '_' + str(value)) )
            sim.run(**params)
        self.runned = True
        self._load()
    
    def _load(self):
        self.simulations = [
            Simulation(child) 
            for child in self.path.iterdir()
            if not (str(child).endswith('_header') or str(child).endswith('.ipynb_checkpoints'))
        ] # Assicuriamoci l'idempotenza

from numpy.random import default_rng
rng = default_rng()

def bootstrap(xx, resample=100, blocksize=None):
    if blocksize is None:
        means = []
        for i in range(resample):
            indexes = rng.integers(low=0, high=len(xx), size=len(xx))
            sample = xx[indexes]
            means.append(np.mean(sample))
        return np.std(means)
    else:
        splits = len(xx)//blocksize
        yy = 1*xx[:splits*blocksize]
        yy = yy.reshape(splits, blocksize)
        ymeans = np.mean(yy, axis=1)
        means = []
        for i in range(resample):
            indexes = rng.integers(low=0, high=len(ymeans), size=len(ymeans))
            sample = ymeans[indexes]
            means.append(np.mean(sample))
        return np.std(means)
"""
def bootstrap(xx, resample=100):
    means = []
    for i in range(resample):
        indexes = rng.integers(low=0, high=len(xx), size=len(xx))
        sample = xx[indexes]
        means.append(np.mean(sample))
    return np.std(means)
"""