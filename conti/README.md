# Conti del modulo 6 di D'ELia

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  

- [Che cosa famo](#che-cosa-famo)
  - [Lagrangiana](#lagrangiana)
  - [Energia dello stato fondamentale](#energia-dello-stato-fondamentale)
  - [Funzione d'onda dello stato fondamentale](#funzione-donda-dello-stato-fondamentale)
- [Come lo famo](#come-lo-famo)
- [SubTasks](#subtasks)
  - [Simulare](#simulare)
  - [Analizzare](#analizzare)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Che cosa famo
Vogliamo trovare a mezzo di path integral, l'effetto Casimir (a temperature anche non nulle. 
Ovvero vogliamo valutare la pressione in due diverse configurazioni e farne la differenza.

Vogliamo in particolare calcolare la pressione al variare della distanza.
Per cui ci servono simulazioni a distanze diverse. 
Ce ne serve poi una a distanze grosse con condizioni periodiche.

### Lagrangiana
Le = 1/2\*[(\partial\_M \phi)^2 + m^2\phi^2]

### Osservabile che dobbiamo campionare
\sum\_n [m^2 \phi^2 + \sum\_\mu (\phi(n+\mu)-\phi(n))^2 - (\phi(n+x)-\phi(n))^2]

dove \mu sono i versori perpendicolari a x

## Come lo famo
Un bellissimo path integral.

## SubTasks

### Simulare
- [ ] Scrivere il codice anche più schifoso che ci sia, ma purché sia veloce da scrivere. 

`g++ field_pressure.cpp -o ../build/field_pressure -std=gnu++17 -ltbb -lfmt -fopenmp -fpermissive -w -O3`

### Analizzare
- [ ] Scrivere gli script per tirare fuori le medie che ci interessano.
- [ ] Aggiustare gli script per tirare fuori le incertezze con Jackknife/bootstrap. 

Poi viene la relazione, ma è un altro paio di maniche

