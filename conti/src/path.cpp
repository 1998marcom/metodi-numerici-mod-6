#ifndef FRAC_ISING_MH
#define FRAC_ISING_MH

#include <iostream>
#include <cstdlib>
#include <random>
#include <fstream>
#include <ctime>
#include <set>
#include <map>
#include <array>
using std::cout, std::endl;


#include <hip/hip_runtime.h>
#include "common.cpp"
#include "mt.cpp"

#ifndef GPU_COMPUTING
#include <cmath>
#endif

#define GRID_FOR(ix, N) for(ul ix=threadIdx.x+blockIdx.x*blockDim.x; ix<N; ix+=gridDim.x*blockDim.x)
#define AT(i, N) ( (i < N) ? ( (i >= 0) ? i : i+N ) : ((i>2*N) ? (i+N) : (i-N)) )

__global__
void metro_dense_kernel_attempt(ModelStruct gpu_model_str, const float p_flip, const flt stddev) {
	GRID_FOR(i, gpu_model_str.size) {
		float randm = random_uniform_01();
		gpu_model_str.new_values[i] = gpu_model_str.values[i];
		if (randm < p_flip) {
			gpu_model_str.new_values[i] += random_normal()*stddev;
		}
	}
}

__forceinline__
__device__ __host__
float potential(const flt value, const float mass) { // HAMILTONIAN HERE
	return value*value*mass;
}

/*
__forceinline__
__device__ __host__
ul coord_to_index(const ModelStruct& model_struct, const ul* crd) {
	return ( (crd[0]*model_struct.Ni+crd[1])*model_struct.Nj + crd[2] ) * model_struct.Nk + crd[3];	
}

__forceinline__
__device__ __host__
void index_to_coord(const ModelStruct& model_struct, ul index, ul* crd) {
	crd[3] = index % model_struct.Nt;
	index = index / model_struct.Nt;
	crd[2] = index % model_struct.Nk;
	index = index / model_struct.Nk;
	crd[1] = index % model_struct.Nj;
	index = index / model_struct.Nj;
	crd[0] = index;
}
*/

	

__global__
void metro_dense_kernel_finalize(ModelStruct gpu_model_str, const float m) {
	unsigned long long size = gpu_model_str.size;
	GRID_FOR(i, size) {
		// Conditions under which nothing occurs
		if (gpu_model_str.values[i] == gpu_model_str.new_values[i]) continue;
        int8_t difference_accumulator = 0; 
        FOR(j, TWO_DIMS) {
            if (j<gpu_model_str.n_links[i]) {
                difference_accumulator =
                    difference_accumulator ||
                    ( gpu_model_str.values[gpu_model_str.linked_ids[j][i]] 
					  != gpu_model_str.new_values[gpu_model_str.linked_ids[j][i]] ) ;
            }
                // da notare che sarebbe come FOR(j, gpu_model_str.n_links[i]), ma probabilmente così è più veloce
        }

		if (not difference_accumulator) {
            // Qua sta il Metropolis
            flt old_value = gpu_model_str.values[i];
            flt new_value = gpu_model_str.new_values[i];

			float old_kin = 0.0;
			float new_kin = 0.0;
			FOR(j, TWO_DIMS) {
				if (j < gpu_model_str.n_links[i]) {
					auto next_value = gpu_model_str.values[gpu_model_str.linked_ids[j][i]];
					old_kin += 0.5 * (next_value - old_value) * (next_value - old_value);
					new_kin += 0.5 * (next_value - new_value) * (next_value - new_value);
				}
			}
			float old_pot = potential(old_value, m);
			float new_pot = potential(new_value, m);
			float old_lagr = old_kin + old_pot;
			float new_lagr = new_kin + new_pot;

            float energy_diff = new_lagr - old_lagr; // HAMILTONIAN HERE (We are writing E2-E1)
            if (energy_diff < 0) {
                gpu_model_str.values[i] = new_value;
                //std::cout << "Flip " << new_value << std::endl;
            } else {
                float metro_p = exp(-energy_diff);
                float randm = random_uniform_01();
                if (randm < metro_p) {
                    gpu_model_str.values[i] = new_value;
                    //std::cout << "Flip " << new_value << std::endl;
                } else {
                    //std::cout << "Flip no " << gpu_model_str.values[i] << std::endl;
                }
            }
        }
	}
}

// Then we would need the Wolff kernels (TODO)

class PathModel {
	private:
		std::mt19937 cpu_random_generator;
		std::uniform_int_distribution<unsigned int> cpu_distribution_unsigned;
		std::uniform_real_distribution<float> cpu_distribution_float_01;
		ModelStruct gpu_model_str;
		bool on_gpu;
		__host__
		void to_gpu() {
			if (on_gpu) return;
			on_gpu = true;
			hipMemcpyHtoD(gpu_model_str.values, cpu_model_str.values, cpu_model_str.size*sizeof(flt));
		}
		__host__
		void to_cpu() {
			if (not on_gpu) return;
			on_gpu = false;
			hipMemcpyDtoH(cpu_model_str.values, gpu_model_str.values, cpu_model_str.size*sizeof(flt));
		}
		bool quit_flag = false;
	public:
		float stddev = 1;
		float m = 1;
		float g = 1;
		ul links = 0;
		ModelStruct cpu_model_str; // it's public so that copying it's not needed, you can access its members and build it in-place
		PathModel(
				const ul& size,
				std::string seed=""
				) : cpu_distribution_float_01(0, 1) {

			cpu_model_str.size = size;
			cpu_model_str.values = malloc(size*sizeof(flt)); // Dio perdoni il malloc, ma dato sotto è hipMalloc...
			cpu_model_str.new_values = malloc(size*sizeof(flt));
			cpu_model_str.n_links = malloc(size*sizeof(uint8_t));
			memset(cpu_model_str.n_links, 0, size*sizeof(uint8_t));
			FOR(i, TWO_DIMS) {
				cpu_model_str.linked_ids[i] = malloc(size*sizeof(ul));
			}

			gpu_model_str.size = size;
			hipMalloc(& gpu_model_str.values, size*sizeof(flt));
			hipMalloc(& gpu_model_str.new_values, size*sizeof(flt));
			hipMalloc(& gpu_model_str.n_links, size*sizeof(uint8_t));
			FOR(i, TWO_DIMS) {
				hipMalloc(& gpu_model_str.linked_ids[i], size*sizeof(ul));
			}

			on_gpu = false;
			init_generators(seed);
			cpu_random_generator.seed(time(NULL));
		}
		void quit() {
			quit_flag = true;
		}
		~PathModel() { 
			if (quit_flag) return; // Segfault stocastico in uscita perché la memoria si libera in async;
			free(cpu_model_str.values); free(cpu_model_str.new_values); free(cpu_model_str.n_links);
            FOR(i, TWO_DIMS) {
                free(cpu_model_str.linked_ids[i]);
            }
            hipFree(gpu_model_str.values); hipFree(gpu_model_str.new_values); hipFree(gpu_model_str.n_links);
            FOR(i, TWO_DIMS) {
                hipFree(gpu_model_str.linked_ids[i]);
            }
		}
		std::set<ul> linked_ids(const ul& x, const ul& distance = 1, const bool& exact=true) {
            std::set<ul> result;
            result.insert(x);
            if (distance > 0) {
                FOR(i, (ul) cpu_model_str.n_links[x]) {
                    std::set<ul> tmp = linked_ids(cpu_model_str.linked_ids[i][x], distance-1);
                    result.insert(tmp.begin(), tmp.end());
                }
            }
            if (exact and distance > 0) {
                std::set<ul> diff = linked_ids(x, distance-1);
                for(auto& el : diff) {
                    result.erase(el);
                }
            }
            return result;
        }
		int safe_link(const ul& x, const ul& y) {
            //cout << "Safe linking " << x << " to " << y << endl;
            unsigned int links_x = cpu_model_str.n_links[x];
            unsigned int links_y = cpu_model_str.n_links[y];
            if (links_x >= TWO_DIMS or links_y >= TWO_DIMS) {
                return -1; // Not enough space to add other links
            } else {
                auto lids = linked_ids(x);
                if (lids.count(y)) {
                    return -2; // Already connected
                } else {
                    cpu_model_str.linked_ids[links_x][x] = y;
                    cpu_model_str.n_links[x] += 1;
                    cpu_model_str.linked_ids[links_y][y] = x;
                    cpu_model_str.n_links[y] += 1;
                    return 0;
                }
            }
        }
		int asymmetric_link(const ul& x, const ul& y) {
            //cout << "Asymmetric linking " << x << " to " << y << endl;
            unsigned int links_x = cpu_model_str.n_links[x];
            if (links_x >= TWO_DIMS) {
                return -1; // Not enough space to add other links
            } else {
                auto lids = linked_ids(x);
                if (lids.count(y)) {
                    return -2; // Already connected
                } else {
                    cpu_model_str.linked_ids[links_x][x] = y;
                    cpu_model_str.n_links[x] += 1;
                    return 0;
                }
            }
        }
		bool test_link(const ul& x, const ul& y) {
            FOR(j, cpu_model_str.n_links[x]) {
                if (cpu_model_str.linked_ids[j][x] == y) return true;
            }
            return false;
        }
		void copy_graph_to_gpu() {
            gpu_model_str.size = cpu_model_str.size;
            hipMemcpyHtoD(gpu_model_str.n_links, cpu_model_str.n_links, cpu_model_str.size*sizeof(uint8_t));
            FOR(i, TWO_DIMS) {
                hipMemcpyHtoD(
					gpu_model_str.linked_ids[i], 
					cpu_model_str.linked_ids[i], 
					cpu_model_str.size*sizeof(ul));
            }
        }
		flt* get_values() {
			to_cpu();
			return cpu_model_str.values;
		}
		void refresh_values() {
			on_gpu = false;
		}
		virtual std::vector<double> get_quantities() { // Feel free to inherit from this class and override as you wish
			return std::vector<double>(1, 0.0);
		}
		__host__
		void step_metro_dense(const float& p_flip) { 
			if (not on_gpu) to_gpu();
			// std::cout << " Metro dense kernel attempt " << std::endl;
			hipLaunchKernelGGL(
					metro_dense_kernel_attempt, dim3(N_BLOCKS), dim3(THREADS_PER_BLOCK),
					0, nullptr,
					gpu_model_str, p_flip, stddev);
			hipDeviceSynchronize();
			// std::cout << " Metro dense kernel finalize " << std::endl;
			hipLaunchKernelGGL(
					metro_dense_kernel_finalize, dim3(N_BLOCKS), dim3(THREADS_PER_BLOCK),
					0, nullptr,
					gpu_model_str, m);
			to_cpu();
			/*
			FOR(i, cpu_model_str.size) {
				cout << cpu_model_str.values[i] << "\t";
			}
			cout <<endl;
			*/
		}

		friend std::ostream& operator<< (std::ostream& stream, PathModel& ising);
		friend double my_observable(PathModel& model, std::array<ul, 4> dims, const std::string& boundaries, const ul x_limit);

};

std::ostream& operator<< (std::ostream& stream, PathModel& ising) {
	std::vector<double> quantities = ising.get_quantities();
	for(const auto& qty: quantities) {
		stream << qty << "\t";
	}
	stream << "\n";
	return stream;
}

#endif
