#include "path.cpp"
#include <random>
#include <cmath>
#include <iostream>
#include <map>
#include <ctime>
#include <vector>
#include <fstream>
#include <string>
#include <sstream>
#include <tclap/CmdLine.h>
#include <fmt/format.h>
#include <vector>

typedef unsigned int uint;
using std::cout, std::endl;
using namespace std;

template <typename T> int sgn(T val) {
	    return (T(0) < val) - (val < T(0));
}

class Coord {
	private:
		array<ul, 4> crd;
		array<ul, 4> size;
	public:
		Coord(const ul& i, const ul& j, const ul& k, const ul& t, const array<ul, 4>& in_size) {
			crd[0] = i; crd[1] = j; crd[2] = k; crd[3] = t;
			size = in_size;
		}
		Coord(const array<ul, 4> in_crd, const array<ul, 4> in_size) {
			size = in_size;
			crd = in_crd;
		}
		Coord(ul index, const array<ul, 4> in_size) {
			size = in_size;
			from_index(index);
		}
		ul to_index() const{
			return ( (crd[0]*size[1]+crd[1])*size[2] + crd[2] ) * size[3] + crd[3];
		}
		void from_index(ul index) {
			crd[3] = index % size[3];
			index = index / size[3];
			crd[2] = index % size[2];
			index = index / size[2];
			crd[1] = index % size[1];
			index = index / size[1];
			crd[0] = index % size[0];
		}
		ul& operator[](const ul& at){
			return crd[at];
		}
		bool overflow() const {
			bool lower_of = ( (crd[0] < 0) or (crd[1] < 0) or (crd[2] < 0) or (crd[3] < 0) );
			bool upper_of = ( 
					(crd[0] >= size[0]) or (crd[1] >= size[1]) or (crd[2] >= size[2]) or (crd[3] >= size[3]) );
			return lower_of or upper_of;
		}
		void renormalize() {
			crd[3] = (crd[3]+size[3]) % size[3];
			crd[2] = (crd[2]+size[2]) % size[2];
			crd[1] = (crd[1]+size[1]) % size[1];
			crd[0] = (crd[0]+size[0]) % size[0];
		}
		array<ul, 4> get_size() const{
			return size;
		}
		ul total_size() const {
			return size[0] * size[1] * size[2] * size[3];
		}
		friend std::ostream& operator<< (std::ostream& stream, const Coord& crd);
};

std::ostream& operator<< (std::ostream& stream, const Coord& crd) {
	FOR(i, 3) {
		stream << crd[i] << " ";
	}
	stream << crd[3];
    return stream;
}

void construct(PathModel& model, const array<ul, 4> dims, const std::string& boundaries, const flt& reference) {
	// Questa function è una cavolo di meraviglia 
	ul total_size = dims[0] * dims[1] * dims[2] * dims[3];
	model.cpu_model_str.values[total_size] = reference;
	//cout <<total_size << " " << model.cpu_model_str.size <<endl;
	FOR(i, total_size) {
		Coord my_coord(i, dims);
		//cout << "Linking from " << i << " with coord: " << my_coord << endl;
		FOR(axis, 4) {
			Coord next_coord = my_coord; next_coord[axis]++;
			if (next_coord.overflow()) {
				switch (boundaries[axis]) {
					case 'L':
						break;
					case 'X':
						model.asymmetric_link(i, total_size);
						//cout << "\tAlong axis " << axis << ", to total_size ("<<total_size<<")"<<endl;
						break;
					case 'P':
						next_coord.renormalize();
						model.safe_link(i, next_coord.to_index());
						//cout << "\tTo coord (P): "<<next_coord<< " ("<< next_coord.to_index() <<")"<<endl;
						break;
				}
			} else {
				model.safe_link(i, next_coord.to_index());
						//cout << "\tTo coord: "<<next_coord<<" ("<< next_coord.to_index() <<")"<<endl;
			}
			next_coord = my_coord; next_coord[axis]--;
			if (next_coord.overflow()) {
				switch (boundaries[axis]) {
					case 'L':
						break;
					case 'X':
						model.asymmetric_link(i, total_size);
						//cout << "\tTo total_size ("<<total_size<<")"<<endl;
						break;
					case 'P':
						next_coord.renormalize();
						model.safe_link(i, next_coord.to_index());
						//cout << "\tTo coord (P): "<<next_coord<<" ("<< next_coord.to_index() <<")"<<endl;
						break;
				}
			} else {
				model.safe_link(i, next_coord.to_index());
						//cout << "\tTo coord: "<<next_coord<<" ("<< next_coord.to_index() <<")"<<endl;
			}
		}
		/* FOR(j, TWO_DIMS) {
			if (j<model.cpu_model_str.n_links[i]) {
				cout << j << " " << model.cpu_model_str.linked_ids[j][i] << endl;
			}
		} */
	}
}

Coord get_next_coord(const Coord& my_coord, const uint& axis, const char& sign, const std::string& boundaries) {
	Coord next_coord = my_coord;
	ul total_size = my_coord.total_size();
	if (sign == '+') { next_coord = my_coord; next_coord[axis]++; }
	else if (sign == '-') { next_coord = my_coord; next_coord[axis]--;}
	else throw 45;

	if (next_coord.overflow()) {
		switch (boundaries[axis]) {
			case 'L':
				next_coord = my_coord; break;
				//if (sign == '+') { next_coord = my_coord; next_coord[axis]--; break; }
				//if (sign == '-') { next_coord = my_coord; next_coord[axis]++; break; }
			case 'X':
				next_coord = Coord(total_size-1, my_coord.get_size()); next_coord[3]++; break;
			case 'P':
				next_coord.renormalize(); break;
		}
	}
	return next_coord;
}
	
double get_fair_scale(array<ul, 4> dims, const std::string& boundaries, const uint axis) {
	if (boundaries[axis] == 'L') {
		return  ((float) dims[axis]) / (dims[axis]-1);
	} else {
		return 1.0;
	}
}

double my_observable(PathModel& model, array<ul, 4> dims, const std::string& boundaries, const ul x_limit = -1) {
	// cout << "My obs" <<endl;
	model.to_cpu();
	ul total_size = dims[0] * dims[1] * dims[2] * dims[3];
	double observable = 0;
	uint x = 3;
	auto values = model.get_values();
	FOR(i, total_size) {
		flt v = values[i];
		observable += 0.5 * (model.m * model.m * v * v);

		Coord my_coord(i, dims);
		if (my_coord[x] >= x_limit)
			continue;

		//double fair_scale = get_fair_scale(dims, boundaries, x);
		double fair_scale = 1.0;
		
		Coord next_coord = get_next_coord(my_coord, x, '+', boundaries);
		double deriv = values[next_coord.to_index()] - values[i];
		observable -= 0.25 * deriv * deriv * fair_scale;
		
		next_coord = get_next_coord(my_coord, x, '-', boundaries);
		deriv = values[next_coord.to_index()] - values[i];
		observable -= 0.25 * deriv * deriv * fair_scale;

		vector<uint> positive_axis = {0, 1, 2};
		for(const auto axis: positive_axis) {
			//fair_scale = get_fair_scale(dims, boundaries, axis);
			next_coord = get_next_coord(my_coord, axis, '+', boundaries);
			deriv = values[next_coord.to_index()] - values[i];
			observable += 0.25 * deriv * deriv * fair_scale;
			
			next_coord = get_next_coord(my_coord, axis, '-', boundaries);
			deriv = values[next_coord.to_index()] - values[i];
			observable += 0.25 * deriv * deriv * fair_scale;
		}
	}
	//float expected_links = model.cpu_model_str.size * TWO_DIMS;
	ul obs_size = Coord(0, dims).total_size();
	if (x_limit < dims[x])
		obs_size = obs_size / dims[x] * x_limit;
	return observable /*expected_links/model.links*/ / obs_size;
}
		
int main(int argc, char* argv[]) {
	
	// argv = [ env_dim, n, extra_occupation, model_scale, sim_time, beta, outfile_name ]
	// model_size = vedi giù
	try {
		TCLAP::CmdLine parser(
"Programma per simulare una teoria libera scalare e prenderne la pressione lungo l'asse x. \
Vedi docs per altre info."
			);
		TCLAP::MultiSwitchArg ARG_q(
				/* flag */ "q",
				/* name */ "quiet",
				/* desc */ "Ask me to be more silent.");
		parser.add(ARG_q);
		TCLAP::ValueArg<double> ARG_m(
				/* flag */ "m",
				/* name */ "mass",
				/* desc */ "Adimensional mass to use in the Lagrangian (default: 1.0).",
				/* req  */ false,
				/* defau*/ 1,
				/* typed*/ "A floating point value");
		parser.add(ARG_m);
		TCLAP::ValueArg<flt> ARG_init_value(
				/* flag */ "v",
				/* name */ "init-value",
				/* desc */ "Initial value for the field (default: 0.0).",
				/* req  */ false,
				/* defau*/ 0.0,
				/* typed*/ "A floating point value");
		parser.add(ARG_init_value);
		TCLAP::ValueArg<std::string> ARG_sim_time(
				/* flag */ "s",
				/* name */ "sim-time",
				/* desc */ 
"Steps of simulation to perform: each step involves a dense Metropolis-Hastings \
(with settable flipping probability). \
Each 10 steps the simulation variables are recorded. Each 100 steps variables are printed on stdout.",
				/* req  */ true,
				/* defau*/ "101",
				/* typed*/ "An unsigned integer");
		parser.add(ARG_sim_time);
		TCLAP::ValueArg<double> ARG_pflip(
				/* flag */ "p",
				/* name */ "pflip",
				/* desc */ "Base probability of flipping for the dense Metropolis-Hastings (default: 0.07).",
				/* req  */ false,
				/* defau*/ 0.07,
				/* typed*/ "A floating point value");
		parser.add(ARG_pflip);
		TCLAP::ValueArg<double> ARG_stddev(
				/* flag */ "d",
				/* name */ "stddev",
				/* desc */ "Standard deviation to use when altering a single point of the path during a MH step (default: 0.1).",
				/* req  */ false,
				/* defau*/ 0.1,
				/* typed*/ "A floating point value");
		parser.add(ARG_stddev);
		TCLAP::ValueArg<std::string> ARG_boundaries(
				/* flag */ "B",
				/* name */ "boundaries",
				/* desc */ 
"Four consecutive characters: each fixes the boundary condition for one axis. \
For each character there are 3 options: L (fLoating), X (fiXed), P (Periodic). \
If you specify fiXed for any axis, you may want to fix the reference value.",
				/* req  */ true,
				/* defau*/ "LLLL",
				/* typed*/ "A string of four characters.");
		parser.add(ARG_boundaries);
		TCLAP::ValueArg<flt> ARG_reference(
				/* flag */ "R",
				/* name */ "reference",
				/* desc */ "The reference value for fiXed boundary conditions.",
				/* req  */ false,
				/* defau*/ 0.0,
				/* typed*/ "A floating point value.");
		parser.add(ARG_reference);
		TCLAP::ValueArg<std::string> ARG_Ni(
				/* flag */ "I",
				/* name */ "Ni",
				/* desc */ "The lattice x dimension (default: 20)",
				/* req  */ false,
				/* defau*/ "20",
				/* typed*/ "A positive integer.");
		parser.add(ARG_Ni);
		TCLAP::ValueArg<std::string> ARG_Nj(
				/* flag */ "J",
				/* name */ "Nj",
				/* desc */ "The lattice y dimension (default:20)",
				/* req  */ false,
				/* defau*/ "20",
				/* typed*/ "A positive integer.");
		parser.add(ARG_Nj);
		TCLAP::ValueArg<std::string> ARG_Nk(
				/* flag */ "K",
				/* name */ "Nk",
				/* desc */ "The lattice z dimension (default: 20)",
				/* req  */ false,
				/* defau*/ "20",
				/* typed*/ "A positive integer.");
		parser.add(ARG_Nk);
		TCLAP::ValueArg<std::string> ARG_Nt(
				/* flag */ "T",
				/* name */ "Nt",
				/* desc */ "The lattice t dimension.",
				/* req  */ true,
				/* defau*/ "20",
				/* typed*/ "A positive integer.");
		parser.add(ARG_Nt);
		TCLAP::ValueArg<std::string> ARG_l(
				/* flag */ "l",
				/* name */ "x-limit",
				/* desc */ "The lattice x dimension until which to take the measure (default: 999'999'999)",
				/* req  */ false,
				/* defau*/ "999999999",
				/* typed*/ "A positive integer.");
		parser.add(ARG_l);
		TCLAP::ValueArg<std::string> ARG_seed(
				/* flag */ "S",
				/* name */ "seed",
				/* desc */ "Use this string as a seed for the pseudo random number generator.",
				/* req  */ false,
				/* defau*/ "CUSUMANO",
				/* typed*/ "A string");
		parser.add(ARG_seed);
		TCLAP::ValueArg<std::string> ARG_outfile(
				/* flag */ "o",
				/* name */ "outfile",
				/* desc */ "Where to output the simulation results",
				/* req  */ false,
				/* defau*/ "NULL",
				/* typed*/ "A relative path");
		parser.add(ARG_outfile);

		parser.parse(argc, argv);

		ul sim_time = stoll(ARG_sim_time.getValue());
		ul Ni = stoll(ARG_Ni.getValue());
		ul Nj = stoll(ARG_Nj.getValue());
		ul Nk = stoll(ARG_Nk.getValue());
		ul Nt = stoll(ARG_Nt.getValue());
		ul l = stoll(ARG_l.getValue());
		std::string boundaries = ARG_boundaries.getValue();
		double stddev = ARG_stddev.getValue();
		double m = ARG_m.getValue();
		flt init_value = ARG_init_value.getValue();
		double pflip = ARG_pflip.getValue();
		flt reference = ARG_reference.getValue();
		std::string seed = ARG_seed.getValue();
		uint quietancy = ARG_q.getValue();

		// Building the model
		ul size = Ni*Nj*Nk*Nt;
		array<ul, 4> dims;
			dims[0] = Ni;
			dims[1] = Nj;
			dims[2] = Nk;
			dims[3] = Nt;
		PathModel model(size+1, seed); // +1 because we might want to usa the fixed field
		construct(model, dims, boundaries, reference);
		model.cpu_model_str.size = size; // Ma poi nella simulazione non vogliamo tocchi il fixed field
		FOR(i, model.cpu_model_str.size) {
			model.links += model.cpu_model_str.n_links[i];
		}
		model.copy_graph_to_gpu();

		// Initializing a non-thermalized state
		FOR(i, size+1) {
			model.cpu_model_str.values[i] = init_value;
		}
		model.refresh_values();

		model.stddev = stddev;
		model.m = m;
		
		std::string outfile_name = ARG_outfile.getValue();
		std::fstream outfile_header;
		std::fstream outfile;
		if (outfile_name != "NULL") {
			outfile_header.open(outfile_name+"_header", std::fstream::out | std::fstream::trunc);
			outfile_header << "I\tJ\tK\tT\tl\ts\tB\t"
							<< "m\tp\td\tS\tR\tv" << endl;
			outfile_header << Ni << " " << Nj << " " << Nk << " " << Nt << " ";
			outfile_header << l << " ";
			outfile_header << sim_time << " " << boundaries << " ";
			outfile_header << m << " ";
			outfile_header << pflip << " ";
			outfile_header << stddev << " ";
			outfile_header << seed << " ";
			outfile_header << reference << " ";
			outfile_header << init_value << " ";
			outfile_header.close();

			outfile.open(outfile_name, std::fstream::in | std::fstream::out | std::fstream::trunc);
		}

		// Simulating
		FOR(t, sim_time) {
			if (t%10 == 0) {
				if (outfile_name != "NULL") {
					outfile << my_observable(model, dims, boundaries, l) << "\n";
				}
			}
			if (not (t%100)) {
				if (not quietancy) {
					cout << "@t=" << t << "\n";
					cout << fmt::format("\tModel\tmy_observable: {:03.15f}\n", my_observable(model, dims, boundaries, l));
				} else {
					if ( not ( t % (10* ((ul) (pow(10,quietancy)+0.5))) ) ) {
						cout << fmt::format("{:08d}\t {:03.15f}\n", t, my_observable(model, dims, boundaries, l));
						/*
						auto post = model.get_values();
						cout << "\t";
						FOR(i, size+1) {
							cout << post[i] << " ";
						}
						cout << endl; */
					}
				}
			}
			model.step_metro_dense(pflip);
		}
		outfile.close();
		
		model.quit();
	} catch (TCLAP::ArgException &e) {
		std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl;
	}
	return 0;
}
