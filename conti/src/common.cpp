#ifndef FRAC_ISING_COMMON
#define FRAC_ISING_COMMON

#include <hip/hip_runtime.h>
#include <cstdint>

//#define GPU_COMPUTING

#ifdef GPU_COMPUTING

// We are on GPU
#define THREADS_PER_BLOCK 64
#define N_BLOCKS 128
	// note that N_BLOCKS < 200 in order for the MTGP32 to work on Nvidia platform

#else

// We are on CPU
#define THREADS_PER_BLOCK 256

#include <thread>
const auto N_BLOCKS = std::thread::hardware_concurrency();


#endif


typedef double flt;
typedef unsigned long long int ul;

#define FOR(i, N) for(ul i=0; i<N; i++)
#define CEIL_DIV(a, b) (a-1)/b+1
// Note that a>0 for CEIL_DIV to work as expected
#define TWO_DIMS 8

enum class BorderCondition {FIXED, FLOATING, PERIODIC}; 

typedef struct{
	ul size; 
	uint8_t* n_links;
	ul* linked_ids[TWO_DIMS];
	
	flt* values;
	flt* new_values;
} ModelStruct;

#endif
