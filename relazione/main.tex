\documentclass[a4paper, 11pt]{article}
%\DeclareMathSizes{10}{11}{9}{8}
\usepackage[top=4cm, bottom=3cm, inner=2.8cm, outer=2.8cm]{geometry}
\usepackage{hyperref}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{float}
\hypersetup{colorlinks, linkcolor=blue}
\usepackage{hyperref}

\newcommand{\dd}{\mathrm{d}}

\title{Effetto Casimir per una teoria scalare libera massless\footnote{Sorgenti e pdf su \url{https://gitlab.com/1998marcom/metodi-numerici-mod-6}}}
\title{
    Metodi Numerici per la Fisica\\
	Effetto Casimir per una teoria scalare libera massless\footnote{
		Sorgenti e pdf su
		\url{https://gitlab.com/1998marcom/metodi-numerici-mod-6}
	}
    \\ \vspace{10pt}
    \small{PROGETTO MODULO 6}\\
}
\author{Marco Malandrone e Alessandro Falco}
%\author{Marco Malandrone\\\textbf{Scuola Normale Superiore\\ \vspace{10pt} Relatore: Prof. Massimo D'Elia}}

\date{\today}

\begin{document}

\maketitle

\tableofcontents

\section{Introduzione}
\subsection{Il modello}
\subsubsection*{Teoria scalare libera}

Riferiamoci a una teoria di campo, in $1+1$ dimensioni, descritta dalla lagrangiana euclidea:
\begin{equation}
	L_E = \frac12\left[(\partial_x\phi)^2 + (\partial_t\phi)^2 + m^2 \phi^2\right]
\end{equation}

Per le simulazioni, discretizziamo lo spazio-tempo. 
Ovvero riferiamoci al caso di un reticolo $N_i\times N_t$. 
Per lavorare con quantità adimensionali (usando $\hbar=c=1$) 
scegliamo come unica quantità dimensionale il passo reticolare $a$.
In particolare definiamo $m = \frac1a \widehat m $ e $\phi = \frac1a \widehat\phi$ 
e riscriviamo la densità lagrangiana come:
\begin{equation}
	L_E = \frac1{2a^2}
		\left[\sum_\mu\left(\widehat\phi(n+\hat\mu)-\widehat\phi(n)\right)^2+\widehat m^2\widehat\phi^2(n)\right]
\end{equation}
\vspace{00pt}
\subsubsection*{Path integral approach}
Nel caso di una teoria euclidea, le diverse possibili configurazioni di campo sono associate a un fattore 
$ \exp\left\{-\int \dd^2\vec x \;L_E(\vec x)\right\}$. Nel caso della teoria discretizzata, questo diventa
$ \exp\left\{-a^2\sum_n  \,L_E(n)\right\}$. 

Ovvero ciascuna configurazione di campo ha un peso dato da:
\begin{equation}
	\exp \left\{ - \frac12 \sum_n 
		\left[\sum_\mu\left(\widehat\phi(n+\hat\mu)-\widehat\phi(n)\right)^2+\widehat m^2\widehat\phi^2(n)\right]
	\right\}
\end{equation}

\subsection{Gli osservabili}
\subsubsection*{Densità di energia interna}

Vogliamo valutare la forza dovuta all'effetto Casimir, a temperature molto prossime allo zero.
Questo caso è confrontabile con risultati analitici. 

Inoltre, in 1+1 dimensioni, la forza (o la pressione) fra le due piastre è:
\begin{equation}
	\mathcal F = \frac{\partial \left(\Delta F\right)}{\partial L} 
		= \frac{\partial \left(\Delta U-T\Delta S\right)}{\partial L}
		\approx \frac{\partial\left(\Delta U \right)}{\partial L}
\end{equation}
dove:
\begin{itemize}
	\item $F$ è l'energia liber di Helmholtz,
	\item $U$ è l'energia interna,
	\item $T$ è la temperatura del sistema,
	\item $S$ è l'entropia del sistema,
	\item $L$ è la lunghezza del sistema, ovvero la distanza fra le due piastre.
\end{itemize}
e dove l'ultima uguaglianza è esatta nel limite $T\rightarrow0$.

Abbiamo allora scelto di misurare la differenza di densità di energia interna $\Delta u = \Delta U/L$,
tra la configurazione in cui il campo è ``confinato'' tra due piastre e la configurazione in cui il campo è,
idealmente, senza vincoli di bordo. Il ``confinamento'' del campo si traduce in condizioni al bordo
in cui il campo deve annullarsi.

Vogliamo ora determinare quale funzionale del campo è associato a all'osservabile $U$.
Ricordando che $U=-\partial_\beta\log Z$, introduciamo un reticolo anisotropo in cui $a_t=\zeta a$;
in questo modo $\beta = N_t\,a_t = \zeta N_t\,a$, mentre per $U$ si ottiene:
\begin{equation}
	U = - \frac1{N_t\,a} \left[\frac1Z \partial_\zeta Z\right]
\end{equation}
L'azione si riscrive come:
\begin{equation}
	S_E = \frac12 \sum_n
		\left[
			\zeta\left(\widehat\phi(n+\hat x)-\widehat\phi(n)\right)^2
			+ \frac1\zeta\left(\widehat\phi(n+\hat t)-\widehat\phi(n)\right)^2
			+\zeta\,\widehat m^2\widehat\phi^2(n)
		\right]
\end{equation}

Per cui:
\begin{align}
	\Delta u &= \frac{\Delta U}{L} = -\frac1{N_iN_ta^2}\;\Delta\left(\frac1Z\partial_\zeta Z\right) \\
	  &= \Delta\left(\frac1{2\beta L} 
	  	\left\langle \sum_n
		\left[
			\left(\widehat\phi(n+\hat x)-\widehat\phi(n)\right)^2
			- \frac1{\zeta^2}\left(\widehat\phi(n+\hat t)-\widehat\phi(n)\right)^2
			+\,\widehat m^2\widehat\phi^2(n)
		\right]
		\right\rangle
		+ K(\zeta, a)\right)
\end{align}
Dove $K(\zeta, a)$ % $= -\frac1{2\zeta a^2}$ 
è una costante legata alla derivata della misura di integrazione.

Scegliamo a questo punto di settare $\zeta = 1$:
\begin{equation}
	\Delta u = \Delta\left(\frac1{2\beta L} 
	  	\left\langle \sum_n
		\left[
			\left(\widehat\phi(n+\hat x)-\widehat\phi(n)\right)^2
			- \left(\widehat\phi(n+\hat t)-\widehat\phi(n)\right)^2
			+\,\widehat m^2\widehat\phi^2(n)
		\right]
		\right\rangle
		+ K(1, a)\right)
\end{equation}

\subsection{La teoria}

Ripercorrendo la derivazione dell'effetto Casimir per il caso di una sola
dimensione spaziale si ottiene:
\begin{equation}
	\Delta U = -\frac{\pi}{24 L}
\end{equation}
ovvero:
\begin{equation}
	\Delta u = -\frac{\pi}{24 L^2} = -\frac{\pi}{24 N_i^2 a^2}
\end{equation}

\section{Implementazione}

Abbiamo simulato il reticolo usando una variante parallelizzata del Metropolis-Hastings 
e usato come generatore di numeri pseudocasuali MT19937,
un generatore del tipo Mersenne-Twister.
La simulazione è implementata in \texttt{C++}; 
il codice sorgente è reperibile nel repository 
\href{https://gitlab.com/1998marcom/metodi-numerici-mod-6}{metodi-numerici-mod-6}.

Il programma di simulazione garantisce la possibilità di scegliere diverse 
dimensioni del reticolo e diverse condizioni al bordo, 
indipendentemente per ogni asse, a \textit{runtime}. Le condizioni al bordo
possono essere fissate, per simulare la presenza delle piastre,
periodiche, per simulare un reticolo ``infinito'', e libere, 
per ridurre il numero di dimensioni utilizzate.

\subsection{Metropolis-Hastings \textit{parallelo}}
La variante del Metropolis-Hastings utilizzata è analoga a quella impiegata
per il progetto dei moduli 1 e 3, ed è opportunamente modificata per essere
applicata al caso delle teorie di campo.
\subsubsection*{L'idea}
Il normale algortimo Metropolis-Hastings consente di far evolvere un sistema cambiando il valore del campo, 
un sito alla volta. 
Nel caso però in cui l'iterazione sia solamente fra siti primi vicini,
è possibile cambiare il valore del campo in più di un sito alla volta, 
prestando attenzione solo a variazioni locali dell'azione.
Tutto, però, a condizione di non cambiare in un'unica \textit{operazione} (nel seguito anche detta \textit{tick}) 
due siti primi vicini.

\subsubsection*{L'algoritmo}
Ciascun \textit{tick} di evoluzione dell'algoritmo può essere diviso in due step. 
Fra questi due step è opportuno assicurarsi che la memoria venga sincronizzata e tutti i core condividano
la stessa memoria in lettura.
\begin{enumerate}
	\item Per ogni sito $n$ del sistema: 
		\begin{itemize}
			\item Si sceglie con probabilità $p$ se cambiare il campo. 
			\item Se si sceglie di cambiare il campo, come nel caso di un Metropolis-Hastings normale,
				si estrae un nuovo valore: $\phi_1(n,t) = \phi(n,t) +\delta$, 
				dove $\phi(n,t)$ è il vecchio valore del campo, 
				$\phi_1(n,t)$ è il possibile nuovo valore del campo al tempo $t+1$
				e $\delta$ è un valore estratto con una distribuzione arbitraria.
			\item Si memorizzano $\phi_1(n, t)$ e $\phi(n, t)$ per lo step 2.
		\end{itemize}
	\item Per ogni sito $n$ del sistema:
		\begin{itemize}
			\item Se nello step 1 si è scelto di non cambiare il campo, assegna $\phi(n, t+1) = \phi(n, t)$.
			\item Se nello step 1 uno qualunque dei siti vicini $m$ ha memorizzato un valore 
				$\phi_1(m,t)\neq\phi(m,t)$, assegna $\phi(n, t+1) = \phi(n, t)$.
			\item Se nessuna delle due precedenti condizioni è verificata, calcola la variazione dell'azione
				$\Delta S$ e assegna $\phi(n, t+1)$ con un algoritmo accept-reject:
				\[
					\phi(n, t+1) = 
						\begin{cases}
							\phi(n, t) &\text{if }\Delta S>0 \text{ with probability}\exp[-\Delta S]\\
							\phi_1(n, t) &\mathrm{otherwise}
						\end{cases}
				\]
		\end{itemize}
\end{enumerate}

\subsubsection*{Verifica del bilancio dettagliato}
La verifica della condizione del bilancio dettagliato discende dalla rispetto della condizione per il normale 
Metropolis-Hastings, dato che è possibile considerare un \textit{tick} del Metropolis-Hastings parallelo come
una concatenazione di passi del normale Metropolis-Hastings in un qualunque ordine, non essendo questi passi in alcun modo dipendenti l'uno dall'altro.

\subsection{Parametri del programma}
Il programma espone alcuni parametri di interesse fisico; 
in particolare ci riferiremo a questi nel seguito con i nomi dei parametri
utilizzati dallo stesso programma:
\begin{itemize}
	\item \texttt{l} o \texttt{x-limit}: Gli osservabili vengono misurati su un
		sottoreticolo costituito da tutti quei punti che hanno coordinata
		$x\leq\texttt{x-limit}$.

	\item \texttt{T} o \texttt{Nt}: Lunghezza lungo $\hat t$ del reticolo.

	\item \texttt{I} o \texttt{Ni}: Lunghezza lungo $\hat x$ del reticolo.

	\item \texttt{B} o \texttt{boundaries}:
		Quattro caratteri consecutivi: ognuno determina le condizioni di bordo
		per ciascuno dei 4 assi (in ordine, $\hat x, \hat y, \hat z, \hat t$).
		Per ogni carattere ci sono tre opzioni: \texttt{L} (fLoating),
		\texttt{X} (fiXed), \texttt{P} (Periodic).

	\item \texttt{s} o \texttt{sim-time}
		Numero di \textit{tick} di evoluzione da simulare. Ogni \texttt{tick} comprende una
		operazione di Metropolis-Hastings parallelo. Le osservabili vengono
		registrate ogni 10 \texttt{tick}.

	\item \texttt{m} o \texttt{mass}
		Massa adimensionale da usare nella lagrangiana.
\end{itemize}

\section{Misure e analisi}
\subsection{Run}

Per simulare la situazione del campo confinato tra due piastre 
abbiamo eseguito una spazzata per diverse lunghezze del sistema,
con condizioni al bordo fissate lungo l'asse $x$ e periodiche lungo l'asse $t$,
e con $N_i \ll N_t=500$ per lavorare nel limite di temperatura nulla.
I parametri sono:
\begin{verbatim}
{   
    '--boundaries': 'XLLP',
    '--Ni': range(5, 26),
    '--Nt': '500',
    '--x-limit': '999999999',
    '--mass': '0',
    '--sim-time': '20000',
}
\end{verbatim}

Per simulare la situazione di campo libero abbiamo simulato il campo 
con condizioni periodiche lungo gli assi $x$ e $t$, e abbiamo limitato i siti utilizzati per calcolare l'osservabile
a quelli con $x<\texttt{x-limit}$. Abbiamo fatto una spazzata per diversi valori di \texttt{x-limit}, corrispondenti
agli stessi valori scelti in precedenza per $N_i$. I parametri della simulazione sono quindi:
\begin{verbatim}
{   
    '--boundaries': 'PLLP',
    '--Ni': 50,
    '--Nt': '500',
    '--stddev': '0.3',
    '--x-limit': range(5, 26),
    '--mass': '0',
    '--sim-time': '10000',
}
\end{verbatim}

\subsection{Misure}
Abbiamo quindi raccolto i diversi valori di $a^2 \,\Delta u$ al variare della distanza tra le piastre $N_i$
e li abbiamo confrontati con la predizione teorica, in figura \ref{fig:th_vs_data}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.83\textwidth]{images/th_vs_data.png}
	\caption{Risultati delle simulazioni confrontati con la predizione teorica.}
	\label{fig:th_vs_data}
\end{figure}
I dati raccolti mostrano un discreto accordo con la predizione teorica; in particolare il $\chi^2$ ridotto vale 
$\chi^2_r \approx 0.85$.

Abbiamo poi provato a vedere se fosse possibile ricostruire la funzione $a^2\,\Delta u(N_i)$ 
a partire dai dati delle simulazioni.
In particolare, abbiamo cercato un \textit{best fit} dei parametri $a, b$ di una funzione $f_{a,b}(N_i) = a\,N_i^b$. 
I risultati sono mostrati in figura \ref{fig:th_vs_data_vs_fit}. 

Come parametri ottimali abbiamo ottenuto $a=-0.10\pm0.05, \; b=-1.78\pm0.25$, 
a fronte di valori teorici di $\bar a=-\pi/24\approx-0.13, \; \bar b=-2$. In questo caso abbiamo ottenuto
$\chi^2/\mathrm{ndof}=0.70$.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.83\textwidth]{images/th_vs_data_vs_fit.png}
	\caption{Dati e teoria confrontati con il la curva di best fit.}
	\label{fig:th_vs_data_vs_fit}
\end{figure}


\end{document}
